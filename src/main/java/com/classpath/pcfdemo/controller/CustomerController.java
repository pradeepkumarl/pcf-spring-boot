package com.classpath.pcfdemo.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.pcfdemo.model.Customer;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
	
	@GetMapping
	public List<Customer> listAll(){
		return Arrays.asList(new Customer(12, "Kiran", 34));
	}
}
