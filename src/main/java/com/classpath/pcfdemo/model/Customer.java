package com.classpath.pcfdemo.model;

public class Customer {
	
	private long id;
	
	private String customerName;
	
	private int customerAge;
	
	

	public Customer(long id, String customerName, int customerAge) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.customerAge = customerAge;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getCustomerAge() {
		return customerAge;
	}

	public void setCustomerAge(int customerAge) {
		this.customerAge = customerAge;
	}

}
